// On document load
window.onload = function(){
    debugSpan = document.getElementById('debugSpan');
    debug("Ready...");
    render();
};

// Debugging
var debugSpan;
function debug(text){
    debugSpan.innerHTML += text + "<br />";
}
function debugClear(){
    debugSpan.innerHTML = "";
}

// Math
function tanAngle(vector){
    var adj = vector.z;
    var opp = vector.x;
    return Math.atan(opp/adj);
}

//
// Creating the scene
//
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 50 );
camera.position.z = 15;

//
// Setting the scene
//

// Add globe
var globe = new THREE.Mesh(
    new THREE.SphereGeometry( 5, 12, 12 ),
    new THREE.MeshBasicMaterial({
        color: 0xFFFFFF,
        map: new THREE.TextureLoader().load("images/earth.jpg")
    })
);
globe.add( new THREE.AxisHelper( 8 ) );
scene.add( new THREE.EdgesHelper( globe, 0xAADDFF ) );
scene.add( globe );

// Add moon
var moon = new THREE.Mesh(
    new THREE.SphereGeometry( 1, 8, 8 ),
    new THREE.MeshBasicMaterial( {color: 0xAAAAAA} )
);
scene.add( new THREE.EdgesHelper( moon, 0x666666 ) );
moon.position.x=8;
globe.add( moon );

//
// Animating the scene
//
function onFrame() {
    debugClear();
    debug("globe "+Math.round(THREE.Math.radToDeg( globe.rotation.y )));

    if (targetAngle){
        var step = 0.01;
        debug("target "+Math.round(THREE.Math.radToDeg(targetAngle)));

        if (globe.rotation.y + step < targetAngle) { // West to East
            globe.rotation.y += step;
            debug("+");
        } else if (globe.rotation.y - step > targetAngle) { // East to West
            globe.rotation.y -= step;
            debug("-");
        } else { // Less than a step, to set to final position and remove target
            globe.rotation.y = targetAngle % THREE.Math.degToRad(360); // Lock to 360 degrees
            targetAngle = null;
        }
    }
}

//
// Interaction
//
var clickArrow;
var targetAngle;

document.addEventListener('mousedown', onDocumentMouseDown, false);
document.addEventListener('touchstart', onDocumentTouchStart, false);
function onDocumentTouchStart(event) {
    event.preventDefault();
    event.clientX = event.touches[0].clientX;
    event.clientY = event.touches[0].clientY;
    onDocumentMouseDown(event);
}

function onDocumentMouseDown(event) {
    event.preventDefault();

    // Create mouse vector
    var mouse = new THREE.Vector2();
    mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

    // Ray-cast from screen to mouse position
    var raycaster = new THREE.Raycaster();
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObject( globe );

    // If there was an intersection
    if (intersects.length > 0) {
        var target = intersects[0];

        // Get click location and convert into a normalised vector from the origin
        var clickVector = globe.worldToLocal(target.point);
        clickVector.normalize();

        // Update clickArrow position
        if (clickArrow) globe.remove(clickArrow);
        clickArrow = new THREE.ArrowHelper(clickVector, new THREE.Vector3( 0, 0, 0 ), 6, 0xffff00);
        globe.add(clickArrow);

        // Get targetVector
        var targetVector = new THREE.Vector3( clickVector.x, 0, clickVector.z ); // clickVector without the y position
        targetVector.normalize(); // Adjust for the removed Y component
        globe.localToWorld(targetVector); // Convert from a position relative to the globe, to a global position
        targetAngle = globe.rotation.y - tanAngle( targetVector ); // Get the target angle
    }
}

//
// Rendering the scene
//
var renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
function render() {
    requestAnimationFrame( render );
    onFrame();
    renderer.render( scene, camera );
}
